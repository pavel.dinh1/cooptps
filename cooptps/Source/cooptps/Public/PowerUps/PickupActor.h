// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupActor.generated.h"

UCLASS()
class COOPTPS_API APickupActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickupActor();

protected:
	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USphereComponent* SphereComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UDecalComponent* DecalComp;

	UPROPERTY(EditInstanceOnly, Category = "PickupActor")
		TSubclassOf<class APowerupActor> PowerUpClass;

	APowerupActor* PowerUpInstance;

	UPROPERTY(EditInstanceOnly, Category = "PickupActor")
		float CooldownDuration;

	FTimerHandle TimerHandle_RespawnTimer;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	void Respawn();
public:

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

};
