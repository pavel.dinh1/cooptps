// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TpsWeapon.h"
#include "TpsProjectile.generated.h"

/**
 * THIS IS A PROJECTILE WEAPON !!
 */
UCLASS()
class COOPTPS_API ATpsProjectile : public ATpsWeapon
{
	GENERATED_BODY()
	
public:
	virtual void Fire() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "ProjectileWeapon")
	TSubclassOf<AActor> ProjectileClass;

};
