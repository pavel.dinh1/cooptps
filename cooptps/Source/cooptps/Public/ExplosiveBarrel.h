// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplosiveBarrel.generated.h"

UCLASS()
class COOPTPS_API AExplosiveBarrel : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AExplosiveBarrel();

protected:
	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UHealthComponent* HealthComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class URadialForceComponent* RadialForceComp;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
		float ExplosionDamage;

	UPROPERTY(ReplicatedUsing = OnRep_Exploded)
		bool bExploaded;

	UPROPERTY(EditDefaultsOnly, Category = "FX")
		float ExplosionImpulse;



	UPROPERTY(EditDefaultsOnly, Category = "FX")
		class UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly, Category = "FX")
		class UMaterialInterface* ExploadedMaterial;
protected:
	UFUNCTION()
		void OnHealthChanged(UHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType,
			class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
		void OnRep_Exploded();
};
