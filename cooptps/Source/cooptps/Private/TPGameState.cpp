// Fill out your copyright notice in the Description page of Project Settings.

#include "TPGameState.h"
#include "Net/UnrealNetwork.h"

void ATPGameState::OnRep_WaveState(EWaveState OldState)
{
	WaveStateChanged(WaveState, OldState);
}


void ATPGameState::SetWaveState(EWaveState NewState)
{
	if (Role == ROLE_Authority)
	{
		EWaveState OldState = WaveState;
		WaveState = NewState;
		// Call on server
		OnRep_WaveState(OldState);
	}
}

void ATPGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPGameState, WaveState);
}
